import theme from 'grey-docs'

export default theme({
    components: true,
    content: {
        liveEdit: false
    },
    docs: {
        primaryColor: '#64748b'
    },
    head: {
        title: 'Learn with Grey Software | Democratizing Software Education!',
        meta: [
            {
                name: 'og:title',
                content: 'Learn with Grey Software | Democratizing Software Education!'
            },
            {
                name: 'og:description',
                content: 'A website where we create useful learning material for the open-source software ecosystem!'
            },
            {
                name: 'og:image',
                content: '/preview.png'
            },
            {
                name: 'twitter:card',
                content: 'summary_large_image'
            },
            {
                name: 'twitter:title',
                content: 'Learn with Grey Software | Democratizing Software Education!'
            },
            {
                name: 'twitter:description',
                content: 'A website where we create useful learning material for the open-source software ecosystem!'
            },
            {
                name: 'twitter:image',
                content: '/preview.png'
            },
        ],
        script: [
            {
                src: 'https://plausible.io/js/plausible.js',
                async: true,
                defer: true,
                'data-domain': 'learn.grey.software',
            },
        ]
    }
})

