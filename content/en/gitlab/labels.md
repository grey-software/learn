---
title: Labels
description: Learn about how we use Gitlab labels to collaborate.
category: Gitlab
position: 10
---

As the number of tasks grow in GitLab, it becomes more challenging to manage them. 

Labels are a powerful, flexible way to categorize our Gitlab tasks.

When applied appropriately and consistently, labels enable GitLab users to organize and tag their work such that they can track and filter the issues they are interested in.

## Labels at Grey Software

### Prority Labels ![](https://i.imgur.com/Ou1y5Ha.png)

### Stage Labels ![](https://i.imgur.com/bNDMC7g.png)

### Status Labels ![](https://i.imgur.com/zgwdRP4.png)

<cta-button text="View Our Labels" link="https://gitlab.com/groups/grey-software/-/labels"> </cta-button>

As mentioned earlier we use labels to categorize our tasks.

## Labels used with Issues

![](https://i.imgur.com/8lEann7.png)

> [We invite you to explore how we use labels with issues](https://gitlab.com/groups/grey-software/-/issues)

## Labels used with Epics 

![](https://i.imgur.com/67zJzmO.png)

> [We invite you to explore how we use labels with Epics](https://gitlab.com/groups/grey-software/-/epics)

## Labels used with Merge requests

![](https://i.imgur.com/JwRHFbU.png)

> [We invite you to explore how we use labels with merge requests](https://gitlab.com/groups/grey-software/-/merge_requests?scope=all&state=opened)
