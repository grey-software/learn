---
title: Comments And Threads 
description: Learn about how we use Gitlab comments and threads to collaborate.
category: Gitlab
position: 14
---

## Overview

GitLab encourages communication through comments, threads, and code suggestions which makes collaboration more easier.

## Comments and Threads at Grey Software

### Comments with Issues 

The following are the comments made under the "[We need to make tweaks to make GreyDocs theme suit our visual design](https://https://gitlab.com/grey-software/templates/grey-docs/-/issues/4)" issue ![](https://i.imgur.com/oiaj3jg.png)

We invite you to explore these issues so you can get a deeper understanding of how to utilize this **'comments and threads'** feature which can greatly help you in making effective comments .

> - [Research ways for users to submit feedback or suggest features on our app](https://gitlab.com/grey-software/focused-browsing/-/issues/33)
> - [We need a product pitch deck](https://gitlab.com/grey-software/focused-browsing/-/issues/48)
> - [Showing/hiding feeds is slow](https://gitlab.com/grey-software/focused-browsing/-/issues/25)

### Comments with Merge requests

> - [Update: refined contributing document](https://gitlab.com/grey-software/resources/-/merge_requests/29)
> - [Updated navigation components](https://gitlab.com/grey-software/templates/grey-docs/-/merge_requests/20)
> - [Ecosystem partnership section added](https://gitlab.com/grey-software/website/-/merge_requests/77)

### Comments with Epics

> - [Raise capital through Founding Partner Packages to Angel Investors and VCs](https://gitlab.com/groups/grey-software/-/epics/24)
