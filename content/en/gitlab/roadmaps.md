---
title: Roadmaps
description: Learn about how we use Gitlab roadmaps to collaborate.
category: Gitlab
position: 13
---

The Roadmap page shows the epics and milestones in a group containing a start date or due date visualized in a form of a timeline (that is, a Gantt chart). <br>

Gitlab roadmaps help us see our plan for the upcoming months and years by giving us a high level overview of our active [epics](/gitlab/epics) and [milestones](/gitlab/milestones).

![](https://i.imgur.com/YQDjvXZ.png)

<cta-button text="View Our Roadmap" link="https://gitlab.com/groups/grey-software/-/roadmap?state=all&sort=start_date_asc&layout=WEEKS&timeframe_range_type=CURRENT_QUARTER"> </cta-button>



