---
title: Markdown Intro
description: Let's introduce you to the Markdown language and why you should learn it.
position: 5
category: Markdown
---

## What is Markdown

Markdown is a lightweight markup language used primarily to:

- Produce print-ready documents 
- Create content for a website
- Take notes

Markdown was developed by John Guber in 2004, and its documents are plain text files that are easy to read and write. 

## Why Learn Markdown

Markdown is among the world's most popular markup languages and translates quickly to other formats like pdf, rich text, and HTML, etc.

Markdown offers a simple way for everyone on the web to communicate without having to rely on HTML or _WYSIWYG_ editors.

<alert>Markdown does not replace HTML. Its syntax corresponds to a small set of HTML tags and doesn't cover [block tags](https://developer.mozilla.org/en-US/docs/Web/HTML/Block-level_elements). Markdown makes it easy to read, write, and edit documents that you can then publish with HTML.</alert>

<alert>
In computing, WYSIWYG is an acronym for What You See Is What You Get. 

Examples of WYSIWYG editors include Microsoft Word and Google Docs!
</alert>

<alert>
For those using a WYSIWYG editor, Markdown allows you to keep your fingers on the keyboard when formatting text, so you don't have to grab the mouse in order to select and format your text before returning to typing.
</alert>
